//
//  BookListTableViewController.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "BookListTableViewController.h"

@interface BookListTableViewController ()
@property (nonatomic, strong) NSMutableDictionary *thumbnails;
@end

@implementation BookListTableViewController

- (void)setActiveBooks:(NSArray *)activeBooks
{
    if (_activeBooks != activeBooks) {
        _activeBooks = activeBooks;
        [self downloadThumbnails];
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)downloadThumbnails
{
    if (!_thumbnails) {
        _thumbnails = [NSMutableDictionary dictionary];
    }
    
    for (Book *book in _activeBooks) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:book.imageLink]];
        [[AFImageDownloader defaultInstance] downloadImageForURLRequest:request withReceiptID:[[NSUUID alloc] initWithUUIDString:book.guid] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
            [_thumbnails setObject:responseObject forKey:[request.URL absoluteString]];
            [self.tableView reloadData];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            NSLog(@"failed to download image");
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _activeBooks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bookCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[BookTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"bookCell"];
    }
    
    Book *book = [_activeBooks objectAtIndex:indexPath.row];
    [cell.title setText:book.title];
    [cell.pubDate setText:book.publicationDate];
    [cell.rating setText:[[NSNumber numberWithFloat:book.rating] stringValue]];
    
    UIImage *thumbnail = [_thumbnails objectForKey:book.imageLink] ? [_thumbnails objectForKey:book.imageLink] : [UIImage imageNamed:@"icon.png"];
    [cell.thumbnail setImage:thumbnail];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
