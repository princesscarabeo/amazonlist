//
//  BookManager.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"

//The manager will send this event every time the book list is udpated
#define EVENT_BOOKMANAGER_BooksUpdated @"EVENT_BOOKMANAGER_BooksUpdated"

@interface BookManager : NSObject

/**
 * Add books to array. If book is already existing, it replaces the original one
 */
- (void)addBooks:(NSArray *)books;

- (NSArray *)getAllBooks;

- (NSArray *)getBooksWithMinimumRating:(float)rating;

- (NSArray *)getBooksWithMaximumRating:(float)rating;

- (NSArray *)getBookWithRangeOfString:(NSString *)subText;

- (NSArray *)getBookWithRangeOfString:(NSString *)subText withMinimumRating:(float)rating;

- (void)clearBooks;

@end
