//
//  AmazonRSSManager.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "AmazonRSSManager.h"

@implementation AmazonRSSManager
- (instancetype)init
{
    self = [super init];
    if (self) {
        _bookManager = [[BookManager alloc]init];
        _requestManager = [[AmazonRSSRequestManager alloc] init];
        [self loadFeedData];
    }
    return self;
}

- (void)loadFeedData
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"RSSList" ofType:@"plist"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    if (dictionary) {
        NSArray *feeds = [dictionary objectForKey:@"RSSFeed"];
        _feeds = [NSArray arrayWithArray:feeds];
    }
}

- (void)downloadFeed{
    [_bookManager clearBooks];
    _requestManager.delegate = self;
    
    for (NSString *feedURL in _feeds) {
        [_requestManager downloadBooksFrom:feedURL];
    }
}
#pragma mark AmazonRSSRequestManagerDelegate
- (void)ARSSRequestManager:(AmazonRSSRequestManager *)rssRequestManger successfullyDownloadedBooks:(NSArray *)books
{
    [_bookManager addBooks:books];
}

- (void)ARSSRequestManagerFailedToDownloadBooks:(AmazonRSSRequestManager *)rssRequestManger
{
    
}
@end
