//
//  AmazonRSSRequestManager.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "AmazonRSSRequestManager.h"

NSString * const kAmazonRootNode = @"channel";
NSString * const kAmazonStartTag = @"<startTag>";
NSString * const kAmazonEndTag = @"</startTag>";

NSString * const kAmazonFeedItemKey = @"item";
NSString *const kAmazonSpanKey = @"span";
NSString * const kAmazonDescriptionKey = @"description";
NSString *const kAmazonUnderscoreTextKey = @"__text";
NSString *const kAmazonUnderscoreHrefKey = @"_href";
NSString *const kAmazonUnderscoreSrc = @"_src";
NSString *const kAmazonRatingBase = @"stars-";
NSString *const kAmazonAHREFKey = @"a";
NSString *const kAmazonBookCoverBaseURL = @"http://ecx.images-amazon.com/images/";

NSString *const kAmazonBookCoverThumbnailSize = @"._SL100";
NSString *const kAmazonBookCoverMaxSize = @"._SL500";
NSString *const kAmazonBookCoverFileExtention = @".jpg";

@interface AmazonRSSRequestManager ()

@end

@implementation AmazonRSSRequestManager
- (void)downloadBooksFrom:(NSString *)url
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    AFXMLParserResponseSerializer *serializer = [AFXMLParserResponseSerializer new];
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/rss+xml"];
    [manager setResponseSerializer:serializer];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            if ([self.delegate respondsToSelector:@selector(ARSSRequestManagerFailedToDownloadBooks:)]){
                [self.delegate ARSSRequestManagerFailedToDownloadBooks:self];
            }
        } else {
            NSLog(@"%@ %@", response, responseObject);
            NSXMLParser *parser = (NSXMLParser *)responseObject;
            NSDictionary *dictionary = [NSDictionary dictionaryWithXMLParser:parser];
            [self parseXMLData:dictionary];
            NSLog(@"%@", dictionary);
        }
    }];
    [dataTask resume];
}

#pragma mark - Private Helpers
- (void)parseXMLData:(NSDictionary *)xmlDictionary
{
    NSMutableArray *feeds = [[NSMutableArray alloc] init];

    NSDictionary * itemDictionary = [xmlDictionary objectForKey:kAmazonRootNode];
    NSArray *items = [itemDictionary objectForKey:kAmazonFeedItemKey];
    for (NSDictionary *currentItem in items){
        NSMutableDictionary *bookData = [NSMutableDictionary dictionary];
        
        NSString *finalXMLString = [NSString stringWithFormat:@"%@%@%@", kAmazonStartTag, [currentItem objectForKey:kAmazonDescriptionKey], kAmazonEndTag];
        NSDictionary *cData = [NSDictionary dictionaryWithXMLString:finalXMLString];
        NSArray *bookDetailsDictionary = [cData objectForKey:kAmazonSpanKey];
        
        
        NSString *bIOURL = [[[[cData objectForKey:@"div"] objectForKey:kAmazonAHREFKey] objectForKey:@"img"] objectForKey:kAmazonUnderscoreSrc];
        NSString *bookImageCoverID = [[[[bIOURL componentsSeparatedByString:kAmazonBookCoverBaseURL] lastObject] componentsSeparatedByString:@"."] firstObject];
        
        
        NSString *bookTitle = [self getBookTitleWithArray:bookDetailsDictionary];
        if ([bookTitle isEqualToString:@""]) {
            bookTitle = [currentItem objectForKey:@"title"]? [currentItem objectForKey:@"title"] : @"";
        }
        NSString *bookAuthor = [self getBookAuthorWithArray:bookDetailsDictionary];
        NSString *bookCoverImageThumbnailURL = [NSString stringWithFormat:@"%@%@%@%@", kAmazonBookCoverBaseURL, bookImageCoverID, kAmazonBookCoverThumbnailSize, kAmazonBookCoverFileExtention];
        NSString *bookCoverImageOriginalURL = [NSString stringWithFormat:@"%@%@%@%@", kAmazonBookCoverBaseURL, bookImageCoverID, kAmazonBookCoverMaxSize, kAmazonBookCoverFileExtention];
        NSString *bookPrice = [self getPriceFromDictionary:cData];
        NSString *bookRating = [self getRatingFromDictionary:cData];
        if ([bookRating isEqualToString:@"0.0"]) {
            //try to get directly from cdata. xmldictionary might have parsing error
            bookRating = [self getRatingFromCData:[currentItem objectForKey:kAmazonDescriptionKey]];
        }
        NSString *bookLink = [self getBookLinkWithArray:bookDetailsDictionary];
        if ([bookLink isEqualToString:@""]) {
            bookLink = [[currentItem objectForKey:@"link"]copy] ? [[currentItem objectForKey:@"link"]copy] : @"";
        }
        NSString *bookPubDate = [[currentItem objectForKey:@"pubDate"] copy];
        NSString *bookGuid = [[[currentItem objectForKey:@"guid"] objectForKey:kAmazonUnderscoreTextKey] copy];
        if (!bookGuid) {
            //use the link as a unique identifier
            bookGuid = bookLink;
        }
        
        
//        if (!bookTitle || !bookAuthor || !bookCoverImageThumbnailURL || !bookCoverImageOriginalURL
//            || !bookPrice || !bookRating || !bookLink || !bookLink || !bookPubDate || !bookGuid) {
//            NSLog(@"ha");
//        }
        
        [bookData setObject:bookTitle forKey:@"title"];
        [bookData setObject:bookAuthor forKey:@"author"];
        [bookData setObject:bookCoverImageThumbnailURL forKey:@"imageLink"];
        [bookData setObject:bookCoverImageOriginalURL forKey:@"originalImageLink"];
        [bookData setObject:bookPrice forKey:@"price"];
        [bookData setObject:bookRating forKey:@"rating"];
        [bookData setObject:bookLink forKey:@"link"];
        [bookData setObject:bookPubDate forKey:@"pubDate"];
        [bookData setObject:bookGuid forKey:@"guid"];
        
        Book *book = [[Book alloc]initWithDictionary:bookData];
        [feeds addObject:book];
    }
    
    NSLog(@"==============================================================");
    for (Book *book in feeds) {
        NSLog(@"%@", book.description);
    }
    NSLog(@"==============================================================");
    
    if ([self.delegate respondsToSelector:@selector(ARSSRequestManager:successfullyDownloadedBooks:)]){
        [self.delegate ARSSRequestManager:self successfullyDownloadedBooks:[feeds copy]];
    }
}

-(NSString*)getBookTitleWithArray:(NSArray*)array{
    if ([array count] <= 0 || ![array isKindOfClass:[NSArray class]]) {
        return @"";
    }
    return [[array[0] objectForKey:kAmazonAHREFKey] objectForKey:kAmazonUnderscoreTextKey];
}

- (NSString *)getBookLinkWithArray:(NSArray *)array{
    if ([array count] <= 0 || ![array isKindOfClass:[NSArray class]]) {
        return @"";
    }
    return [[array[0] objectForKey:kAmazonAHREFKey] objectForKey:kAmazonUnderscoreHrefKey];
}

-(NSString*)getBookAuthorWithArray:(NSArray*)array{
    if ([array count] <= 1 || ![array isKindOfClass:[NSArray class]]) {
        return @"";
    }
    id bookAuthor = [[array[1] objectForKey:kAmazonAHREFKey] objectForKey:kAmazonUnderscoreTextKey];
    
    if(!bookAuthor){
        bookAuthor = [array[1] objectForKey:kAmazonUnderscoreTextKey];
    }
    
    if([bookAuthor isKindOfClass:[NSArray class]]){
        bookAuthor = [bookAuthor componentsJoinedByString:@" "];
    }
    
    return (bookAuthor? bookAuthor:@"");
}
-(NSString*)getPriceFromDictionary:(NSDictionary*)dictionary{
    if (![[dictionary allKeys]containsObject:@"font"]) {
        return @"0";
    }
    
    if (![[dictionary objectForKey:@"font"] isKindOfClass:[NSArray class]]) {
        return [NSString stringWithUTF8String:[[[dictionary objectForKey:@"font"]objectForKey:@"b"] cStringUsingEncoding:NSUTF8StringEncoding]];
    }
    
    return [NSString stringWithUTF8String:[[[[dictionary objectForKey:@"font"] lastObject] objectForKey:@"b"] cStringUsingEncoding:NSUTF8StringEncoding]];
}

-(NSString*)getRatingWithCurrentRatingDictionary:(NSDictionary*)ratingDictionary{
    NSString * stars;
    if([ratingDictionary objectForKey:kAmazonUnderscoreSrc]){
        NSString * possibleStarsURL = [ratingDictionary objectForKey:kAmazonUnderscoreSrc];
        if([possibleStarsURL rangeOfString:kAmazonRatingBase options:NSCaseInsensitiveSearch].location != NSNotFound){
            stars = [[[[[possibleStarsURL componentsSeparatedByString:kAmazonRatingBase] lastObject] componentsSeparatedByString:@"."] firstObject] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        }
    }
    return stars;
}
-(NSString*)getRatingFromDictionary:(NSDictionary*)dictionary{
    id item = [dictionary objectForKey:@"img"];
    NSString *rating;
    if([item isKindOfClass:[NSArray class]]){
        NSArray *list = (NSArray *)item;
        for (NSDictionary *currentRatingDictionary in list){
            if((rating = [self getRatingWithCurrentRatingDictionary:currentRatingDictionary])){
                break;
            }
        }
    }
    else if([item isKindOfClass:[NSDictionary class]]){
        rating = [self getRatingWithCurrentRatingDictionary:item];
    }
    
    if(!rating) rating = @"0.0";
    return rating;
}

- (NSString *)getRatingFromCData:(NSString *)cData
{
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:cData options:0 range:NSMakeRange(0, [cData length])];
    
    NSString *stars;
    for(int i=0; i<matches.count;i++){
        NSTextCheckingResult *result = [matches objectAtIndex:i];
        NSString *linkUrl = [result URL].absoluteString;
        
        if([[linkUrl pathExtension] isEqualToString:@"gif"]){
            if ([linkUrl rangeOfString:kAmazonRatingBase options:NSCaseInsensitiveSearch].location != NSNotFound) {
                stars = [[[[[linkUrl componentsSeparatedByString:kAmazonRatingBase] lastObject] componentsSeparatedByString:@"."] firstObject] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
                break;
            }
        }
    }
    if (!stars) stars = @"0.0";
    return stars;
}

@end
