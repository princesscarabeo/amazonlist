//
//  PlistSerializer.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PlistSerializer <NSObject>

- (id)initWithDictionary:(NSDictionary *)data;
- (void)updateWithDictionary:(NSDictionary *)data;
- (NSDictionary *)serialize;

@end
