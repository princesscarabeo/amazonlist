//
//  AmazonRSSManager.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AmazonRSSRequestManager.h"
#import "BookManager.h"

@interface AmazonRSSManager : NSObject <AmazonRSSRequestManagerDelegate>

@property (nonatomic, readonly) BookManager *bookManager;
@property (nonatomic, readonly) AmazonRSSRequestManager *requestManager;
@property (nonatomic, readonly) NSArray *feeds;

/**
 * Loads the RSSList config
 */
- (void)loadFeedData;

/**
 * Download the Feed for every url in config
 */
- (void)downloadFeed;

@end
