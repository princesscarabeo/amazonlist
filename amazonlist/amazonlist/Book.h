//
//  Book.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "PlistSerializer.h"

@interface Book : NSObject <PlistSerializer>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *publicationDate;
@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic) float rating;



@end
