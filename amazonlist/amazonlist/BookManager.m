//
//  BookManager.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "BookManager.h"

@interface BookManager()
//do not use this to set books.
@property (nonatomic, strong) NSMutableArray *books;
@end

@implementation BookManager
- (instancetype)init
{
    self = [super init];
    if (self) {
        _books = [NSMutableArray array];
    }
    return self;
}

- (void)addBooks:(NSArray *)books
{
    if (books.count<1) {
        return;
    }
    
    for (Book *book in books) {
        Book *potentialDuplicate = [self getBookWithGuid:book.guid];
        if (!potentialDuplicate) {
            //book does not exist yet
            [_books addObject:book];
            NSLog(@"Book with id: %@ aded to list!", book.guid);
        }else {
            NSLog(@"Book with id: %@ duplicate! Updating book from list!", book.guid);
            [_books replaceObjectAtIndex:[_books indexOfObject:potentialDuplicate] withObject:book];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:EVENT_BOOKMANAGER_BooksUpdated object:self];
}

- (Book*)getBookWithGuid:(NSString *)guid
{
    for (Book *book in _books) {
        if ([book.guid.lowercaseString isEqualToString:guid.lowercaseString]) {
            return book;
        }
    }
    return NULL;
}

- (NSArray *)getAllBooks
{
    return [_books copy];
}

- (NSArray *)getBooksWithMinimumRating:(float)rating
{
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Book *book, NSDictionary *bindings) {
        return book.rating >= rating;
    }];
    NSArray *filteredArray = [NSArray arrayWithArray:[_books filteredArrayUsingPredicate:filter]];
    return filteredArray;
}

- (NSArray *)getBooksWithMaximumRating:(float)rating
{
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Book *book, NSDictionary *bindings) {
        return book.rating <= rating;
    }];
    NSArray *filteredArray = [NSArray arrayWithArray:[_books filteredArrayUsingPredicate:filter]];
    return filteredArray;
}

- (NSArray *)getBookWithRangeOfString:(NSString *)subText
{
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Book *book, NSDictionary *bindings) {
        return [book.title.lowercaseString containsString:subText.lowercaseString];
    }];
    NSArray *filteredArray = [NSArray arrayWithArray:[_books filteredArrayUsingPredicate:filter]];
    return filteredArray;
}

- (NSArray *)getBookWithRangeOfString:(NSString *)subText withMinimumRating:(float)rating
{
    NSArray *booksWithRatingFilter = [self getBooksWithMinimumRating:rating];
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Book *book, NSDictionary *bindings) {
        return [book.title.lowercaseString containsString:subText.lowercaseString];
    }];
    NSArray *filteredArray = [NSArray arrayWithArray:[booksWithRatingFilter filteredArrayUsingPredicate:filter]];
    return filteredArray;
}

- (void)clearBooks
{
    [_books removeAllObjects];
}
@end
