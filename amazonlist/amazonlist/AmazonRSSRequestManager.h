//
//  AmazonRSSRequestManager.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "AFNetworking/AFNetworking.h"
#import "XMLDictionary.h"

extern NSString * const kAmazonRootNode;
extern NSString * const kAmazonStartTag;
extern NSString * const kAmazonEndTag;

extern NSString * const kAmazonFeedItemKey;
extern NSString *const kAmazonSpanKey;
extern NSString * const kAmazonDescriptionKey;
extern NSString *const kAmazonUnderscoreTextKey;
extern NSString *const kAmazonUnderscoreHrefKey;
extern NSString *const kAmazonUnderscoreSrc;
extern NSString *const kAmazonRatingBase;
extern NSString *const kAmazonAHREFKey;
extern NSString *const kAmazonBookCoverBaseURL;

extern NSString *const kAmazonBookCoverThumbnailSize;
extern NSString *const kAmazonBookCoverMaxSize;
extern NSString *const kAmazonBookCoverFileExtention;

@class AmazonRSSRequestManager;

@protocol AmazonRSSRequestManagerDelegate <NSObject>
- (void)ARSSRequestManager:(AmazonRSSRequestManager *)rssRequestManger successfullyDownloadedBooks:(NSArray *)books;
- (void)ARSSRequestManagerFailedToDownloadBooks:(AmazonRSSRequestManager *)rssRequestManger;
@end

@interface AmazonRSSRequestManager : NSObject <NSXMLParserDelegate>

@property (nonatomic, weak) id <AmazonRSSRequestManagerDelegate> delegate;

- (void)downloadBooksFrom:(NSString *)url;
@end
