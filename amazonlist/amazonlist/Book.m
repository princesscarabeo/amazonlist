//
//  Book.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "Book.h"

@implementation Book

- (id)initWithDictionary:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        [self updateWithDictionary:data];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)data
{
    _title = [[data objectForKey:@"title"] copy];
    _publicationDate = [[data objectForKey:@"pubDate"] copy];
    _imageLink = [[data objectForKey:@"imageLink"] copy];
    _link = [[data objectForKey:@"link"] copy];
    _rating = [[data objectForKey:@"rating"]floatValue];
    _guid = [data objectForKey:@"guid"];
}

- (NSDictionary *)serialize
{
    NSMutableDictionary * data = [NSMutableDictionary dictionary];
    
    [data setObject:_title forKey:@"title"];
    [data setObject:_publicationDate forKey:@"pubDate"];
    [data setObject:_imageLink forKey:@"imageLink"];
    [data setObject:_link forKey:@"link"];
    [data setObject:[[NSNumber numberWithFloat:_rating] stringValue] forKey:@"rating"];
    [data setObject:_guid forKey:@"guid"];
    
    return [data copy];
}

- (NSString *)description
{
    NSString *desc = [NSString stringWithFormat:@"Book: %@\n === %@ \n link: %@ imagelink: %@ rating: %f guid: %@", _title, _publicationDate, _link, _imageLink,_rating, _guid];
    return desc;
}

@end
