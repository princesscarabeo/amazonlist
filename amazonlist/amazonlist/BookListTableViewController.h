//
//  BookListTableViewController.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "BookTableViewCell.h"
#import "AFImageDownloader.h"

@interface BookListTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *activeBooks;

@end
