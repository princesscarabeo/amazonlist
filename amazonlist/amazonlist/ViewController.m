//
//  ViewController.m
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSString *currentSearchText;
}
@end

@implementation ViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:EVENT_BOOKMANAGER_BooksUpdated];
}

- (void)setRssManager:(AmazonRSSManager *)rssManager
{
    [[NSNotificationCenter defaultCenter]removeObserver:EVENT_BOOKMANAGER_BooksUpdated];
    _rssManager = rssManager;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onBookUpdated:) name:EVENT_BOOKMANAGER_BooksUpdated object:_rssManager.bookManager];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"bookListTableView"]) {
        _bookListTVC = (BookListTableViewController *)segue.destinationViewController;
    }
}

#pragma mark - Event
- (void)onBookUpdated:(NSNotification *)notification
{
    NSArray *books = [NSArray array];
    if (!currentSearchText) {
        currentSearchText = @"";
    }
    if ([currentSearchText isEqualToString:@""]) {
//        books = [_rssManager.bookManager getBooksWithMinimumRating:4.5];
    }else {
        books = [_rssManager.bookManager getBookWithRangeOfString:currentSearchText withMinimumRating:4.5];
        if (books.count<1) {
            NSLog(@"Showing 4.0 because there are no 4.5");
            books= [_rssManager.bookManager getBookWithRangeOfString:currentSearchText withMinimumRating:4.0];
        }
    }
    [_bookListTVC setActiveBooks:[self sortBooks:books]];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [_searchBar becomeFirstResponder];
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    currentSearchText = @"";
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    currentSearchText = searchText;
    if ([currentSearchText isEqualToString:@""]) {
        [_bookListTVC setActiveBooks:[NSArray array]];
    }else {
        //assign old values first
        NSArray *books = [_rssManager.bookManager getBookWithRangeOfString:currentSearchText withMinimumRating:4.5];
        if (books.count<1) {
            NSLog(@"Showing 4.0 because there are no 4.5");
            books= [_rssManager.bookManager getBookWithRangeOfString:currentSearchText withMinimumRating:4.0];
        }
        [_bookListTVC setActiveBooks:[self sortBooks:books]];
        [_rssManager downloadFeed];
    }
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setText:@""];
    [_bookListTVC setActiveBooks:[NSArray array]];
    [searchBar resignFirstResponder];
}

- (NSArray *)sortBooks:(NSArray *)books
{
    NSSortDescriptor *firstDescriptor = [[NSSortDescriptor alloc] initWithKey:@"rating" ascending:NO];
    NSSortDescriptor *secondDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:firstDescriptor, secondDescriptor, nil];
    NSArray *sortedArray = [books sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}

@end
