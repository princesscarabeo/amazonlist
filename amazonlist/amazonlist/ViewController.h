//
//  ViewController.h
//  amazonlist
//
//  Created by Princess Carabeo on 14/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmazonRSSManager.h"
#import "BookListTableViewController.h"

@interface ViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, readonly) BookListTableViewController *bookListTVC;
@property (nonatomic, weak) AmazonRSSManager *rssManager;

@end

